package com.perpus.perpustakaan.services;

import com.perpus.perpustakaan.entities.Books;

import java.util.List;

public interface BookServices {
    public abstract List<Books> getAllBook();
    public abstract List<Books> listbook();
}
