package com.perpus.perpustakaan.services;

import com.perpus.perpustakaan.entities.Books;
import com.perpus.perpustakaan.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Service

public class BookServicesImpl implements BookServices {
    @Autowired
    private BookRepository bookRepo;
    @Override
    public List<Books> getAllBook(){
        bookRepo.findAll();
        return null;
    }
    private List<Books> listbooks=new ArrayList<Books>();
    @Override
    public List<Books> listbook(){
        return listbooks;
    }
    public List<Books> getAll(){
        List<Books> listbooks=List.of(
                    new Books(323,"BUku Mantra","Harry", LocalDate.of(190,Month.AUGUST,23)),
                    new Books(354,"BUku sapu","Budi", LocalDate.of(2000,Month.APRIL,5)),
                    new Books(423,"Buku matematika", "Putra", LocalDate.of(2019,Month.DECEMBER,2)),
                    new Books(546,"Buku Resep ","Ibu ana",LocalDate.of(2023,Month.JANUARY,3)),
                    new Books(234,"BUku Rumus", "pak ahon",LocalDate.of(2020,Month.JULY,9)))  ;
        for(Books books: listbooks){
            listbooks.add(books);
        }
        return listbooks;
    }
    public String add(Books books){
        Books b =new Books();
        b.setAuthor(books.getAuthor());
        b.setBookName(books.getBookName());
        b.setIdbook(books.getIdbook());
        b.setYear(books.getYear());

      listbooks.add(b);
        return String.valueOf(b);

    }
}
