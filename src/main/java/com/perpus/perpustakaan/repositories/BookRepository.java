package com.perpus.perpustakaan.repositories;

import com.perpus.perpustakaan.entities.Books;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Repository
public interface BookRepository extends JpaRepository<Books, UUID> {
   // List<Books> findAll();

}
