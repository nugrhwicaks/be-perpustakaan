package com.perpus.perpustakaan.controller;

import com.perpus.perpustakaan.entities.Books;
import com.perpus.perpustakaan.services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/api")
public class BookController {
    @Autowired
    private BookServices bookServices;

    @GetMapping(value="/AllBook")
    public ResponseEntity<List<Books>> showBooks(){
        List<Books> tampil= bookServices.getAllBook();

        return new ResponseEntity<>(tampil, HttpStatus.OK);

    }
}
