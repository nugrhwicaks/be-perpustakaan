package com.perpus.perpustakaan.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.time.LocalDate;
import java.util.UUID;

@Entity

public class Books {
    @Id
    private Integer idbook;
    private String bookName;
    private String author;
    private LocalDate year;

    public Books(){

    }

    public Books(Integer idbook, String bookName, String author, LocalDate year) {
        this.idbook = idbook;
        this.bookName = bookName;
        this.author = author;
        this.year = year;
    }


    public int getIdbook() {
        return idbook;
    }

    public int setIdbook(int idbook) {
        return idbook;
    }

    public String getBookName() {
        return bookName;
    }

    public String setBookName(String bookName) {
        return bookName;
    }

    public String getAuthor() {
        return author;
    }

    public String setAuthor(String author) {
        return author;
    }

    public LocalDate getYear() {
        return year;
    }

    public LocalDate setYear(LocalDate year) {
        return year;
    }
}
